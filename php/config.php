<?php
/**
 * Virtuo - Virtual User Objects frontend in PHP5 with SQLite3 and PDO
 * Copyright (c) 2007 Andraž Levstik, Elisamuel Resto
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @package Virtuo
 * @author Andraž 'ruskie' Levstik <ruskie@mages.ath.cx>
 * @author Elisamuel 'ryuji' Resto <ryuji@mages.ath.cx>
 * @copyright Copyright (c) 2007, Andraž 'ruskie' Levstik
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link http://ruskie.dtdm.org/virtuo/ Virtuo Website
 * @version 1.0-alpha
 */

/**
 * This is defined to check if we're in Virtuo
 */
 define("VIRTUO_CORE", true);

/**
 * Define our version
 */
 define("VIRTUO_VERSION", '1.0-alpha');

/**
 * Our authentication type
 * Supported: HTTP, HTML
 */
 define("VIRTUO_AUTH", "HTML");

/**
 * Define our DB type. (only sqlite works)
 * Supported: sqlite, mysql, pgsql
 */
 define("VIRTUO_DBTYPE", "sqlite");

/**
 * Our database hostname
 */
 define("VIRTUO_DBHOST", "");

/**
 * Our database username
 */
 define("VIRTUO_DBUSER", "");

/**
 * Our database password
 */
 define("VIRTUO_DBPASS", "");

/**
 * Our database name itself (path *and* filename for sqlite)
 */
 define("VIRTUO_DBNAME", "");

/**
 * Our table prefix (if any)
 */
 define("VIRTUO_DBPREF", "");
?>
