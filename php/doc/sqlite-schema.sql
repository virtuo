CREATE TABLE users (
  id TINYINT PRIMARY KEY,
  username VARCHAR(255),
  password VARCHAR(255),
  uid TINYINT(5),
  gid TINYINT(5),
  gecos VARCHAR(255),
  home VARCHAR(255),
  shell VARCHAR(255),
  home_ftp VARCHAR(255),
  home_mail VARCHAR(255),
  home_vcard VARCHAR(255),
  domain VARCHAR(255),
  is_admin TINYINT(1),
  is_enabled TINYINT(1)
);
