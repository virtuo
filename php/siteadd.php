<html><head><title>Virtuo: Manage Domains</title>
  
    
    <link rel="stylesheet" href="css/style.css" type="text/css"></head><body onload="document.siteadd.domain.focus()">
    <div id="Header"><p><a href="http://ruskie.dtdm.org/virtuo/" target="_blank">Virtuo</a> --  </p></div>    <div id="menu">
      <a href="site.php">Manage Domains</a><br>
      <a href="sitepassword.php">Site Password</a><br>
      <br><a href="logout.php">Logout</a><br>
    </div>
    <div id="forms">
      <form name="siteadd" method="post" action="siteaddsubmit.php">
        <table align="center">
          <tbody><tr>
            <td>New Domain:</td>
            <td><input name="domain" class="textfield" type="text"></td>
            <td>
              The name of the new domain you are adding            </td>
          </tr>
                    <tr>
            <td>Domain Admin:</td>
            <td>
              <input name="localpart" value="postmaster" class="textfield" type="text">
            </td>
            <td>
              The username of the domain's administrator account            </td>
          </tr>
          <tr>
            <td>Password:</td>
            <td>
              <input name="clear" class="textfield" type="password">
            </td>
          </tr>
          <tr>
            <td>Verify Password:</td>
            <td>
              <input name="vclear" class="textfield" type="password">
            </td>
          </tr>
          <tr>
            <td>System UID:</td>
            <td>
              <input name="uid" class="textfield" value="110" type="text">
            </td>
          </tr>
          <tr>
            <td>System GID:</td>
            <td>
              <input name="gid" class="textfield" value="110" type="text">
            </td>
          </tr>
          <tr>
            <td>Domain Mail directory:</td>
            <td>
              <input name="maildir" class="textfield" value="/srv/mail/" type="text">
            </td>
            <td>
              Create the domain directory below this top-level
                  mailstore            </td>
          </tr>
          <tr>
            <td>
              Maximum accounts<br>
              (0 for unlimited):
            </td>
            <td>
              <input size="5" name="max_accounts" value="0" class="textfield" type="text">
            </td>
          </tr>
          <tr>
            <td>
              Max mailbox quota              (0 for disabled):
            </td>
            <td>
              <input name="quotas" size="5" class="textfield" value="0" type="text">Mb            </td>
          </tr>
          <tr>
            <td>Maximum message size:</td>
            <td>
              <input name="maxmsgsize" size="5" class="textfield" value="0" type="text">Kb            </td>
            <td>
              The maximum size for incoming mail (user
                tunable)            </td>
          </tr>
          <tr>
            <td>Spamassassin tag score:</td>
            <td>
              <input name="sa_tag" size="5" class="textfield" value="2" type="text">
            </td>
            <td>
              The score at the "X-Spam-Flag: YES" header will
                be added            </td>
          </tr>
          <tr>
            <td>Spamassassin refuse score:</td>
            <td>
              <input name="sa_refuse" size="5" class="textfield" value="5" type="text">
            </td>
            <td>The score at which to refuse potentially spam
              mail and not deliver            </td>
          </tr>
          <tr>
            <td>Spamassassin enabled?</td>
            <td>
              <input name="spamassassin" class="textfield" type="checkbox">
            </td>
          </tr>
          <tr>
            <td>Anti Virus enabled?</td>
            <td>
              <input name="avscan" class="textfield" type="checkbox">
            </td>
          </tr>
          <tr>
            <td>Enable piping mail to command?</td>
            <td>
              <input name="pipe" class="textfield" type="checkbox">
            </td>
          </tr>
          <tr>
            <td>Domain enabled?</td>
            <td>
              <input name="enabled" class="textfield" checked="checked" type="checkbox">
            </td>
          </tr>
          <tr><td></td></tr>
                  <tr>
            <td>
            </td>
            <td>
              <input name="type" value="local" type="hidden">
              <input name="admin" value="1" type="hidden">
              <input name="submit" value="Submit" type="submit">
            </td>
          </tr>
        </tbody></table>
      </form>
    </div>
  <!-- Layout and CSS tricks obtained from http://www.bluerobot.com/web/layouts/ -->
</body></html>
