<html><head><title>Virtuo: Manage Users</title>
  
    
    <link rel="stylesheet" href="css/style.css" type="text/css"></head><body onload="document.userchange.realname.focus()">
  <div id="Header"><p><a href="http://ruskie.dtdm.org/virtuo/" target="_blank">Virtuo</a> -- mages.ath.cx </p></div>    <div id="menu">
      <a href="adminuser.php">Manage Accounts</a><br>
      <a href="admin.php">Main Menu</a><br>
      <br><a href="logout.php">Logout</a><br>
    </div>
    <div id="forms">
    <input name="user_id" value="3" class="textfield" type="hidden">
        <input name="user_id" value="3" class="textfield" type="hidden">
        <input class="textfield" value="ruskie" name="localpart" type="hidden">
        <table align="center">
      <form name="userchange" method="post" action="adminuserchangesubmit.php"></form>
        <tbody><tr>
          <td>Name:</td>
          <td>
            <input size="25" name="realname" value="Andraž Levstik" class="textfield" type="text">
          </td>
        </tr>
        <tr>
          <td>Email Address:</td>
          <td>ruskie@mages.ath.cx</td>
        </tr>
        <tr>
          <td>Password:</td>
          <td>
            <input size="25" name="clear" class="textfield" type="password">
          </td>
        </tr>
        <tr>
          <td>Verify Password:</td>
          <td>
            <input size="25" name="vclear" class="textfield" type="password">
          </td>
        </tr>
                  <tr>
            <td>UID:</td>
            <td>
              <input size="25" name="uid" class="textfield" value="110" type="text">
            </td>
          </tr>
          <tr>
            <td>:</td>
            <td>
              <input size="25" name="gid" class="textfield" value="110" type="text">
            </td>
          </tr> 
          <tr>
            <td colspan="2" style="padding-bottom: 1em;">
              When you update the UID or GID, please make sure
                your MTA still has permission to create the required user
                directories!            </td>
          </tr>
                  <tr>
            <td>Pipe to command or alternative Maildir:</td>
            <td>
              <input size="25" name="smtp" class="textfield" value="/srv/mail/mages.ath.cx/ruskie/Maildir" type="textfield">
            </td>
          </tr>
          <tr>
            <td colspan="2" style="padding-bottom: 1em;">
              Optional:
              Pipe all mail to a command (e.g. procmail).              <br>
              Check box below to enable:
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <input name="on_piped" type="checkbox">
            </td>
          </tr>
                <tr>
          <td>
            Admin:</td>
            <td>
              <input name="admin" class="textfield" type="checkbox">
            </td>
          </tr>
                <tr>
          <td>Maximum message size:</td>
          <td>
            <input size="5" name="maxmsgsize" value="0" class="textfield" type="text">Kb
          </td>
        </tr>
        <tr>
          <td>Enabled:</td>
          <td><input name="enabled" checked="checked" class="textfield" type="checkbox">
          </td>
        </tr>
        <tr>
          <td>Vacation on:</td>
          <td><input name="on_vacation" type="checkbox">
          </td>
        </tr>
        <tr>
          <td>Vacation message:</td>
          <td>
            <textarea name="vacation" cols="40" rows="5" class="textfield">
</textarea>
          </td>
        </tr><tr>
          <td>Forwarding on:</td>
          <td><input name="on_forward" type="checkbox">
          </td>
        </tr>
        <tr>
          <td>Forward mail to:</td>
          <td>
            <input size="25" name="forward" value="" class="textfield" type="text"><br>
            Must be a full e-mail address!<br>
            OR:<br>
            <select name="forwardmenu">
              <option selected="selected" value=""></option>
                              <option value="abuse@mages.ath.cx">
                  abuse                  (abuse@mages.ath.cx)
                </option>
                              <option value="ruskie@mages.ath.cx">
                  Andraž Levstik                  (ruskie@mages.ath.cx)
                </option>
                              <option value="andreja@mages.ath.cx">
                  Andreja Levstik                  (andreja@mages.ath.cx)
                </option>
                              <option value="calendar@mages.ath.cx">
                  calendar                  (calendar@mages.ath.cx)
                </option>
                              <option value="cron@mages.ath.cx">
                  Crony                  (cron@mages.ath.cx)
                </option>
                              <option value="tlx@mages.ath.cx">
                  Daniel Knabl                  (tlx@mages.ath.cx)
                </option>
                              <option value="postmaster@mages.ath.cx">
                  Domain Admin                  (postmaster@mages.ath.cx)
                </option>
                              <option value="ryuji@mages.ath.cx">
                  Elisamuel Resto                  (ryuji@mages.ath.cx)
                </option>
                              <option value="ivan.kocijan@mages.ath.cx">
                  Ivan Kocijan                  (ivan.kocijan@mages.ath.cx)
                </option>
                              <option value="iuso@mages.ath.cx">
                  Juuso Alasuutari                  (iuso@mages.ath.cx)
                </option>
                              <option value="lynx@mages.ath.cx">
                  lynx                  (lynx@mages.ath.cx)
                </option>
                              <option value="monit@mages.ath.cx">
                  monit                  (monit@mages.ath.cx)
                </option>
                              <option value="odin@mages.ath.cx">
                  odin                  (odin@mages.ath.cx)
                </option>
                              <option value="rss@mages.ath.cx">
                  rss                  (rss@mages.ath.cx)
                </option>
                          </select>
          </td>
        </tr>
        <tr>
          <td>Store Forwarded Mail Locally:</td>
          <td><input name="unseen" type="checkbox">
          </td>
        </tr>
        <tr>
          <td colspan="2" class="button">
            <input name="submit" value="Submit" type="submit">
          </td>
        </tr>
        <tr>
          <td colspan="2" style="padding-top: 1em;">
          Aliases to this account:<br>
                  </td></tr>
      
    </tbody></table>
    <table align="center">
      <form name="blocklist" method="post" action="adminuserblocksubmit.php"></form>
        <tbody><tr>
          <td colspan="2">
            Add a new header blocking filter for this user:
          </td>
        </tr>
        <tr>
          <td>Header:</td>
          <td>
            <select name="blockhdr" class="textfield">
              <option value="From">From:</option>
              <option value="To">To:</option>
              <option value="Subject">Subject:</option>
              <option value="X-Mailer">X-Mailer:</option>
            </select>
          </td>
          <td>
            <input name="blockval" size="25" class="textfield" type="text">
            <input name="user_id" value="3" type="hidden">
            <input name="localpart" value="ruskie" type="hidden">
            <input name="color" value="black" type="hidden">
          </td>
        </tr>
        <tr>
          <td>
            <input name="submit" value="Submit" type="submit">
          </td>
        </tr>
      
    </tbody></table>
    <table align="center">
      <tbody><tr>
        <th>Delete</th>
        <th>Blocked Header</th>
        <th>Content</th>
      </tr>
          </tbody></table>
    </div>
  <!-- Layout and CSS tricks obtained from http://www.bluerobot.com/web/layouts/ -->
</body></html>
