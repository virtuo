<?php
/**
 * Virtuo - Virtual User Objects frontend in PHP5 with SQLite3 and PDO
 * Copyright (c) 2007 Andraž Levstik, Elisamuel Resto
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @package Virtuo
 * @author Andraž 'ruskie' Levstik <ruskie@mages.ath.cx>
 * @author Elisamuel 'ryuji' Resto <ryuji@mages.ath.cx>
 * @copyright Copyright (c) 2007, Andraž 'ruskie' Levstik
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link http://ruskie.dtdm.org/virtuo/ Virtuo Website
 * @version 1.0-alpha
 */

require_once "config.php";
require_once "include/backend.php";
require_once "include/toolbox.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Virtuo</title>
  <link rel="stylesheet" href="css/style.css" type="text/css">
 </head>

 <body onload="document.login.localpart.focus()">
  <div id="Centered">
   <form style="margin-top: 3em;" name="login" method="post" action="login.php">
    <table align="center">
     <tbody>
      <tr>
       <td>Username:</td>
       <td><input name="localpart" class="textfield" type="text"> @ </td>
       <td>
        <select name="domain" class="textfield">
         <option value="ryuji.myftp.org">ryuji.myftp.org</option>
        </select>
       </td>
      </tr>
      <tr>
       <td>Password:</td>
       <td><input name="crypt" class="textfield" type="password"></td>
      </tr>
      <tr>
       <td colspan="3" style="text-align: center; padding-top: 1em;">
        <input name="submit" value="Submit" class="longbutton" type="submit">
       </td>
      </tr>
     </tbody>
    </table>
   </form>
  </div>
 </body>
</html>
<!-- Layout and CSS tricks obtained from http://www.bluerobot.com/web/layouts/ -->
