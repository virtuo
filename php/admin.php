<html><head><title>Virtuo: Virtual User Objects</title>
  
    
    <link rel="stylesheet" href="css/style.css" type="text/css"></head><body>
    <div id="Header"><p><a href="http://ruskie.dtdm.org/virtuo/" target="_blank">Virtuo</a> -- mages.ath.cx </p></div>    <div id="Centered">
      <table align="center">
        <tbody><tr>
          <td>
            <a href="adminuser.php">
              Add, delete and manage POP/IMAP accounts            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="adminalias.php">
              Add, delete and manage aliases, forwards and a Catchall            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="admingroup.php">
              Add, delete and manage groups            </a>
          </td>
        </tr>
        <tr>
          <td>
            <a href="adminfail.php">
              Add, delete and manage :fail:'s            </a>
          </td>
        </tr>
          <tr><td><a href="adminlists.php">Manage mailing lists</a></td></tr>        <tr>
          <td style="padding-top: 1em;">
            <a href="logout.php">Logout</a>
          </td>
        </tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>Domain data:</td></tr><tr><td>magii.eu.org is an alias of mages.ath.cx</td></tr>      </tbody></table>
    </div>
  <!-- Layout and CSS tricks obtained from http://www.bluerobot.com/web/layouts/ -->
</body></html>
