<?php
// SQLite DB
$dbase = "/etc/virtuo/virtuo.sdb";
// un-comment this to disable
//exit();

$username = ((isset($_POST['REMOTE_USER']) && !empty($_POST['REMOTE_USER'])) ? $_POST['REMOTE_USER'] : "");
$user_value = " value=\"".$username."\"";

$html_start = <<<EOF
<!DOCTYPE html PUBLIC "-//DTD//HTML 4.01 Transitional//EN">
<html>
 <head>
  <title>Virtuo passwd</title>
 </head>

 <body>
  <center>
EOF;

$form = <<<EOF
   <form method="post" action="passwd.php">
    User <em>(user@host)</em>: <input type="text" name="username"$user_value /><br />
    Current Password: <input type="password" name="cur_password" /><br />
    New Password: <input type="password" name="new_password" /><br />
    New Password <em>(repeat)</em>: <input type="password" name="new_password2" /><br />
    <input type="submit" name="submit" /> <input type="reset" />
   </form>
EOF;

$html_end = <<<EOF
  </center>
 </body>
</html>
EOF;

function print_html($msg) {
    global $html_start, $form, $html_end;

    return print($html_start.$msg.$form.$html_end);
}

function exception_handler($exception) {
    print_html($exception->getMessage()."<br /><br />\n");
}
set_exception_handler('exception_handler');

// must me a submitted form via POST
if (isset($_POST['submit'])) {
    // username cannot be blank
    if (!isset($_POST['username']) || empty($_POST['username'])) {
        print_html("<span style=\"font-weight:bold;color:#ff0000\">Must specify a username</span><br /><br />\n");
    }

    // current password cannot be blank
    if (!isset($_POST['cur_password']) || empty($_POST['cur_password'])) {
        print_html("<span style=\"font-weight:bold;color:#ff0000\">Must specify the current password</span><br /><br />\n");
    }

    // new password cannot be blank
    if (empty($_POST['new_password']) || empty($_POST['new_password2']) || ($_POST['new_password'] !== $_POST['new_password2'])) {
        print_html("<span style=\"font-weight:bold;color:#ff0000\">Passwords empty or do not match!</span><br /><br />\n");
    }

    $db = new PDO("sqlite:".$dbase);

    // get the username@domain.tld
    unset($domain, $username);
    list($username, $domain) = explode("@", $_POST['username'], 2);

    // username and domain canot be blank
    if (empty($username) || empty($domain)) {
        print_html("<span style=\"font-weight:bold;color:#ff0000\">Username must be in the user@domain.tld format</span><br /><br />\n");
    }

    // check the user exists in the db, taking precautions (return exacly ONE entry)
    $qry = $db->query(sprintf("SELECT id,password FROM users WHERE username='%s' AND domain='%s' LIMIT 0,1", sqlite_escape_string($username), sqlite_escape_string($domain)));
    if ($ret = $qry->fetch(PDO::FETCH_ASSOC)) {
        if (md5($_POST['cur_password']) == $ret['password']) {
            $num = $db->exec(sprintf("UPDATE users SET password='%s' WHERE id='%s'", md5($_POST['new_password']), $ret['id']));

            if ($num > 0) {
                print_html("<strong>Ok - record(s) updated</strong><br /><br />\n");
            } else {
                print_html("<span style=\"font-weight:bold;color:#ff0000\">Fail - record(s) not updated!</span><br />".print_r($db->errorInfo(),1)."<br />\n");
            }
        } else {
            print_html("<span style=\"font-weight:bold;color:#ff0000\">Password is incorrect!</span><br /><br />\n");
        }
    } else {
        print_html("<span style=\"font-weight:bold;color:#ff0000\">User does not exist!</span><br /><br />\n");
    }
} else {
    print_html('');
}
