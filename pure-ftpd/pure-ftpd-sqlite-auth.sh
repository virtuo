#!/bin/dash
VIRTUO_DB=${1:-/etc/virtuo/virtuo.sdb}
AUTHD_USERNAME=$(echo $AUTHD_ACCOUNT | cut -d @ -f 1)
AUTHD_DOMAIN=$(echo $AUTHD_ACCOUNT | cut -d @ -f 2)
if [ "$(sqlite3 $VIRTUO_DB "SELECT '1' FROM users WHERE username = \"$AUTHD_USERNAME\" AND domain = \"$AUTHD_DOMAIN\";")" != "1" ]; then
  echo "auth_ok:0"
  elif [ "$(sqlite3 $VIRTUO_DB "SELECT '1' FROM users WHERE username = \"$AUTHD_USERNAME\" AND domain = \"$AUTHD_DOMAIN\" AND home_ftp != '';")" != "1" ]; then
    echo "auth_ok:-1"
  elif [ "$(sqlite3 $VIRTUO_DB "SELECT '1' FROM users WHERE username = \"$AUTHD_USERNAME\" AND password = \"$(echo -n $AUTHD_PASSWORD | md5sum | cut -c-32)\" AND domain = \"$AUTHD_DOMAIN\" AND home_ftp != '';")" != "1" ]; then
    echo "auth_ok:-1"
  else
    echo "auth_ok:1"
    echo "uid:300"
    echo "gid:300"
    echo "dir:$(sqlite3 $VIRTUO_DB "SELECT home_ftp FROM users WHERE username = \"$AUTHD_USERNAME\" AND password = \"$(echo -n $AUTHD_PASSWORD | md5sum | cut -c-32)\" AND domain = \"$AUTHD_DOMAIN\" AND home_ftp != '';")"
fi
echo "end"

